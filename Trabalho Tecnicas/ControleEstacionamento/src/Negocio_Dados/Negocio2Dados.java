/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio_Dados;

import Negocio.NegocioException;
import Negocio.Ticket;
import java.util.List;

/**
 *
 * @author Alex
 */
public interface Negocio2Dados {
    /*boolean adicionarFuncionarioDTO(Funcionario f) throws DAOSQLException;

    boolean adicionarTicketDTO(Ticket t) throws DAOSQLException;

    boolean adicionarCartaoEspDTO(CartaoEsp c) throws DAOSQLException;

    public boolean adicionarCartaoUsoDTO(CartaoUso c) throws DAOSQLException;
      
    boolean adicionarPagamentoDTO(Pagamento c) throws DAOSQLException;
*/
    public List<Ticket> getTickets() throws NegocioException;

    public Ticket GenerateTicket() throws NegocioException;

    public boolean LiberaTicket(Ticket t) throws NegocioException;
    
    public double getTotalEstadiaDia(String data) throws NegocioException;
    
    public double getTotalEstadiaMes(String mes, String ano) throws NegocioException;
    
    public int getNroTicketsPagosDia(String data) throws NegocioException;
    
    public int getNroTicketsPagosMes(String mes, String ano) throws NegocioException;
    
    public int getNroTicketEspDia(String data) throws NegocioException;
    
    public int getNroTicketEspMes(String mes, String ano) throws NegocioException;
    
    public int getNroTicketEspFuncionario(String func) throws NegocioException;

    public boolean podeLiberar(String user, String cartao) throws NegocioException;

    public void registrarEntrada(String Cartao, String user) throws NegocioException;

    public boolean podeLiberar(String ticket_cartao) throws NegocioException;

    public String gerarFuncionario(String nome) throws NegocioException;

    public String getLastTicket() throws NegocioException;

    public List<Funcionario> getFuncs()throws NegocioException;

    public void pagou(Ticket t, double total) throws NegocioException;


    public void createCartaoEsp(Funcionario funcionario) throws NegocioException;
}
