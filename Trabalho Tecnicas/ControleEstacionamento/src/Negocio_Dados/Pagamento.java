/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio_Dados;

import sun.util.calendar.LocalGregorianCalendar;
import sun.util.calendar.LocalGregorianCalendar.Date;

/**
 *
 * @author Brike
 */
public class Pagamento {
    private int id;
    private double vlrPagto;
    private LocalGregorianCalendar.Date data;

    public Pagamento(int id, double vlrPagto, Date data) {
        this.id = id;
        this.vlrPagto = vlrPagto;
        this.data = data;
    }

    public Date getData() {
        return (Date)data.clone();
    }

    public int getId() {
        return id;
    }

    public double getVlrPagto() {
        return vlrPagto;
    }
    
    public String getDataString(){
        return data.getYear()+"-"+data.getMonth()+"-"+data.getDayOfMonth();
    }
    
    public String getTimeString(){
        return data.getHours()+":"+data.getMinutes()+":"+data.getSeconds();
    }
    
    
}
