/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio_Dados;

import Dados.FuncionarioDTO;
import Dados.TicketDTO;
import Negocio.Ticket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alex
 */
public class convercao {

    static List<Ticket> toViewTickets(List<TicketDTO> tickets) {
        ArrayList<Ticket> ret = new ArrayList<>();
        for(TicketDTO t : tickets){
            ret.add(toViewTicket(t));
        }
        return ret;
    }

    static Ticket toViewTicket(TicketDTO t) {
        return new Ticket(t.getId(), t.getDataEmissaolong(), t.getHoraEmissaolong());
    }

    static TicketDTO toModel(Ticket t) {
        return new TicketDTO(t.getCodigo(), t.getDataEntradaLong(), t.getHoraEntradaLong(),' ');
    }
    
    static List<Funcionario> toViewFuncs(List<FuncionarioDTO> list){
        ArrayList<Funcionario> ret = new ArrayList<>();
        for(FuncionarioDTO f : list){
            ret.add(convercao.toView(f));
        }
        
        return ret;
    }

    public static Funcionario toView(FuncionarioDTO f) {
        return new Funcionario(f.getId(), f.getNome());
    }
    
    public static FuncionarioDTO toModelFunc(Funcionario f){
        return new FuncionarioDTO(f.getId(), f.getNome());
    }
}
