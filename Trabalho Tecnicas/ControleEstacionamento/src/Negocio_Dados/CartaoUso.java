/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio_Dados;

import java.util.Date;


/**
 *
 * @author Brike
 */
class CartaoUso {
    private int cod_esp;
    private int cod_func;
    private String isEntrada;
    private Date data;
private Date hora;

    public Date getHora() {
        return(Date) hora.clone();
    }

    public String getIsEntrada() {
        return isEntrada;
    }

    public CartaoUso(int cod_esp, int cod_func, String isEntrada, Date data, Date hora) {
        this.cod_esp = cod_esp;
        this.cod_func = cod_func;
        this.isEntrada = isEntrada;
        this.data = data;
        this.hora = hora;
    }

    public Date getData() {
        return (Date) data.clone();
    }

    public int getCod_esp() {
        return cod_esp;
    }

    public int getCod_func() {
        return cod_func;
    }
        public long getDataLong(){
        return data.getTime();
    }
    
    public long getTimeLong(){
        return hora.getTime();
    }
    
}
