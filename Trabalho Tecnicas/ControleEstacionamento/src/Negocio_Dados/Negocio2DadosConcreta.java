/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio_Dados;

import Dados.*;
import Negocio.NegocioException;
import Negocio.Ticket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alex
 */
public class Negocio2DadosConcreta implements Negocio2Dados{

    private IDAOSQLDB dao;
    private static Negocio2DadosConcreta instance;
    public static Negocio2DadosConcreta getInstance() throws NegocioException{
        if(instance == null)
            instance = new Negocio2DadosConcreta();
        return instance;
    }
    
    private Negocio2DadosConcreta() throws NegocioException{
        try{
     dao = DAOSQLDB.getInstance();   
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public List<Ticket> getTickets() throws NegocioException {
        try{
        List<Ticket> ret = convercao.toViewTickets(dao.getTickets());
        return ret;}
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public Ticket GenerateTicket() throws NegocioException {
        try{
        Ticket ret = convercao.toViewTicket(dao.GenerateTicket());
        return ret;
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public boolean LiberaTicket(Ticket t) throws NegocioException {
        try{
         return dao.LiberaTicket(convercao.toModel(t));
        }
        catch(DAOSQLExcep e){
            throw  new NegocioException(e.getMessage());
        }
        
    }

    @Override
    public double getTotalEstadiaDia(String data) throws NegocioException {
        try{
        return dao.getTotalEstadiaDia(data);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public double getTotalEstadiaMes(String mes, String ano) throws NegocioException {
        try{
        return dao.getTotalEstadiaMes(mes, ano);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public int getNroTicketsPagosDia(String data) throws NegocioException {
        try{
        return dao.getNroTicketsPagosDia(data);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public int getNroTicketsPagosMes(String mes, String ano) throws NegocioException {
        try{
        return dao.getNroTicketsPagosMes(mes, ano);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public int getNroTicketEspDia(String data) throws NegocioException {
        try{
        return dao.getNroTicketEspDia(data);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public int getNroTicketEspMes(String mes, String ano) throws NegocioException {
        try{
            return dao.getNroTicketEspMes(mes, ano);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public int getNroTicketEspFuncionario(String func) throws NegocioException {
        try{
            return dao.getNroTicketEspFuncionario(func);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public boolean podeLiberar(String user, String cartao) throws NegocioException {
        try{
            return dao.podeLiberar(user, cartao);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public void registrarEntrada(String Cartao, String user) throws NegocioException {
        try{
            dao.registrarEntrada(Cartao, user);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public boolean podeLiberar(String ticket_cartao) throws NegocioException {
        try{
            return dao.podeLiberar(ticket_cartao);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public String gerarFuncionario(String nome) throws NegocioException {
        try{
            return dao.gerarFuncionario(nome);
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public String getLastTicket() throws NegocioException {
        try{
            return dao.getLastTicket();
        }
        catch(DAOSQLExcep e){
            throw  new  NegocioException(e.getMessage());
        }
    }

    @Override
    public List<Funcionario> getFuncs() throws NegocioException{
        try{
        return convercao.toViewFuncs(dao.getFuncs());
        }
        catch(DAOSQLExcep e)
        {
            throw new NegocioException(e.getMessage());
        }
    }

    @Override
    public void pagou(Ticket t, double total) throws NegocioException{
        try{
            dao.pagou(convercao.toModel(t), total);
        }
        catch(DAOSQLExcep e){
            throw new NegocioException(e.getMessage());
        }
    }

    @Override
    public void createCartaoEsp(Funcionario funcionario) throws NegocioException {
        try {
            dao.createCartaoEsp(convercao.toModelFunc(funcionario));
        } catch (DAOSQLExcep ex) {
            throw new NegocioException(ex.getMessage(), ex);
        }
    }
    
}
