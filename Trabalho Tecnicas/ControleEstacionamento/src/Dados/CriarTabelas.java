/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Brike
 */
public abstract class CriarTabelas {
    
    public static void CriarTabelas() throws DAOSQLExcep{      
        dropDB();
        
        createFuncionario();
        createTicket();
        createPagamento();
        createCartaoEsp();
        createCartaoUso();
        
        
        
    }
    
     private static void dropDB() throws DAOSQLExcep {
        try {
            Connection con = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            Statement sta = con.createStatement();
            
            String sql = "";
            
            sql = "DROP TABLE CARTAO_USO ";
            sta.executeUpdate(sql);
            sql = "DROP TABLE CARTAO_ESP ";
            sta.executeUpdate(sql);
            sql = "DROP TABLE FUNCIONARIO ";
            sta.executeUpdate(sql);

            sql = "DROP TABLE PAGAMENTO ";
            sta.executeUpdate(sql);
            sql = "DROP TABLE TICKET " ;
            sta.executeUpdate(sql);
            
            sta.close();
            con.close();
        } catch (SQLException ex) {
            throw new DAOSQLExcep(ex.getMessage());
        }
    }
    
        private static void createTicket() throws DAOSQLExcep {
        try {
            Connection con = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            Statement sta = con.createStatement();
            
            String sql = "CREATE TABLE TICKET ("
                    + "ID INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 4, INCREMENT BY 1),"
                    + "DATAEMISAO DATE NOT NULL,"
                    + "HORAEMISAO TIME NOT NULL,"
                    + "LIBERADO CHAR(1) NOT NULL"
                    + ")";
            sta.executeUpdate(sql);
            sta.close();
            con.close();
        } catch (SQLException ex) {
            throw new DAOSQLExcep(ex.getMessage()+"\nErro ao criar a tabela: TICKET");
        }
    }

    private static void createFuncionario() throws DAOSQLExcep {
        try {
            Connection con = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            Statement sta = con.createStatement();

            String sql = "CREATE TABLE FUNCIONARIO ("
                    + "COD INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
                    + "NOME VARCHAR(100) NOT NULL"
                    + ")";
            sta.executeUpdate(sql);
            sta.close();
            con.close();
        } catch (SQLException ex) {
            throw new DAOSQLExcep(ex.getMessage() + "\nErro ao criar a tabela: FUNCIONARIO");
        }
    }

                
     private static void createPagamento() throws DAOSQLExcep {
        try {
            Connection con = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            Statement sta = con.createStatement();
            
            String sql = "CREATE TABLE PAGAMENTO ("
                    + "ID_TICKET INTEGER NOT NULL PRIMARY KEY ,"
                    + "VALOR NUMERIC(6,2) NOT NULL,"
                    + "DATA_PGTO DATE NOT NULL,"
                    + "HORA_PGTO TIME NOT NULL,"
                    + "FOREIGN KEY (ID_TICKET) REFERENCES TICKET(ID)"
                    + ")";
            sta.executeUpdate(sql);
            sta.close();
            con.close();
        } catch (SQLException ex) {
            throw new DAOSQLExcep(ex.getMessage()+"\nErro ao criar a tabela: PAGAMENTO");
        }
    }
                     
     private static void createCartaoEsp() throws DAOSQLExcep {
        try {
            Connection con = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            Statement sta = con.createStatement();
            
            String sql = "CREATE TABLE CARTAO_ESP ("
                    + "COD_ESP INTEGER NOT NULL ,"
                    + "COD_FUNC INTEGER NOT NULL ,"
                    + "PRIMARY KEY (COD_ESP, COD_FUNC),"
                    + "FOREIGN KEY (COD_FUNC) REFERENCES FUNCIONARIO(COD)"
                    + ")";
            sta.executeUpdate(sql);
            sta.close();
            con.close();
        } catch (SQLException ex) {
            throw new DAOSQLExcep(ex.getMessage()+"\nErro ao criar a tabela: CARTAO_ESP");
        }
    }
     
     private static void createCartaoUso() throws DAOSQLExcep {
        try {
            Connection con = DriverManager.getConnection("jdbc:derby:derbyDB;create=true");
            Statement sta = con.createStatement();
            
            String sql = "CREATE TABLE CARTAO_USO("
                    + "COD_ESP INTEGER NOT NULL ,"
                    + "COD_FUNC INTEGER NOT NULL ,"
                    + "DATA_PGTO DATE NOT NULL,"
                    + "HORA TIME NOT NULL,"
                    + "IS_ENTRADA CHAR(1) NOT NULL, "
                    + "FOREIGN KEY (COD_ESP, COD_FUNC) REFERENCES CARTAO_ESP(COD_ESP, COD_FUNC)"
                    + ")";
            sta.executeUpdate(sql);
            sta.close();
            con.close();
        } catch (SQLException ex) {
            throw new DAOSQLExcep(ex.getMessage()+"\nErro ao criar a tabela: CARTAO_USO");
        }
    }
}
