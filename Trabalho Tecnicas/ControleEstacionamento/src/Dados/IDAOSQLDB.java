/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;

import Negocio.Ticket;
import java.util.List;


/**
 *
 * @author Brike
 */
public interface IDAOSQLDB {
  /*  
     //@Override
    boolean adicionarFuncionarioDTO(FuncionarioDTO f) throws DAOSQLException;

    boolean adicionarTicketDTO(Ticket t) throws DAOSQLException;

    boolean adicionarCartaoEspDTO(CartaoEspDTO c) throws DAOSQLException;

    boolean adicionarCartaoUsoDTO(CartaoUsoDTO c) throws DAOSQLException;
      
    boolean adicionarPagamentoDTO(PagamentoDTO c) throws DAOSQLException;
*/
    public List<TicketDTO> getTickets() throws DAOSQLExcep;

    public TicketDTO GenerateTicket() throws DAOSQLExcep;

    public boolean LiberaTicket(TicketDTO t) throws DAOSQLExcep;
    
    public double getTotalEstadiaDia(String data) throws DAOSQLExcep;
    
    public double getTotalEstadiaMes(String mes, String ano) throws DAOSQLExcep;
    
    public int getNroTicketsPagosDia(String data) throws DAOSQLExcep;
    
    public int getNroTicketsPagosMes(String mes, String ano) throws DAOSQLExcep;
    
    public int getNroTicketEspDia(String data) throws DAOSQLExcep;
    
    public int getNroTicketEspMes(String mes, String ano) throws DAOSQLExcep;
    
    public int getNroTicketEspFuncionario(String func) throws DAOSQLExcep;

    public boolean podeLiberar(String user, String cartao) throws DAOSQLExcep;

    public void registrarEntrada(String Cartao, String user) throws DAOSQLExcep;

    public boolean podeLiberar(String ticket_cartao) throws DAOSQLExcep;

    public String gerarFuncionario(String nome) throws DAOSQLExcep;

    public String getLastTicket() throws DAOSQLExcep;

    public List<FuncionarioDTO> getFuncs() throws DAOSQLExcep;

    public void pagou(TicketDTO toModel, double total) throws DAOSQLExcep;

    public void createCartaoEsp(FuncionarioDTO toModelFunc) throws DAOSQLExcep;



    
       
}

    

