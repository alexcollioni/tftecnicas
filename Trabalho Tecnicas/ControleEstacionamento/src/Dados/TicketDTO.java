/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;

import java.util.Date;


/**
 *
 * @author Brike
 */
public class TicketDTO {
    
    private int id;
    
    private Date horaEmissao;
    private Date dataEmissao;
    private char liberado;

    public TicketDTO(int id, long horaEmissao, long dataEmissao, char liberado) {
        this.id = id;
        this.horaEmissao = new Date( horaEmissao);
        this.dataEmissao = new Date( dataEmissao);
        this.liberado = liberado;
    }

    

    public int getId() {
        return id;
    }

    public char getLiberado() {
        return liberado;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public Date getHoraEmissao() {
        return horaEmissao;
    }
    
    public long getDataEmissaolong() {
        return dataEmissao.getTime();
    }

    public long getHoraEmissaolong() {
        return horaEmissao.getTime();
    }
    
    
}
