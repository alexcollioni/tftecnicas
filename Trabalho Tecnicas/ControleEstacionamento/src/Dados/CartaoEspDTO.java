/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;

/**
 *
 * @author Brike
 */
class CartaoEspDTO {
    
    private int cod;
    private int cod_func;

    public CartaoEspDTO(int cod, int cod_func) {
        this.cod = cod;
        this.cod_func = cod_func;
    }

    public int getCod() {
        return cod;
    }

    public int getCod_func() {
        return cod_func;
    }
    
}
