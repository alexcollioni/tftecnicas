/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Brike
 */
public class DAOSQLDB implements IDAOSQLDB {

    private static DAOSQLDB ref;

    public static DAOSQLDB getInstance() throws DAOSQLExcep {
        if (ref == null) {
            ref = new DAOSQLDB();
        }
        return ref;
    }

    private DAOSQLDB() throws DAOSQLExcep {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException ex) {
            throw new DAOSQLExcep("JDBC Driver não Encontrado!");
        }

        //CRIA TODAS AS TABELAS
        //CriarTabelas.CriarTabelas();
    }
    

    private static Connection getConnection() throws SQLException {
        //derbyDB sera o nome do diretorio criado localmente
        return DriverManager.getConnection("jdbc:derby:derbyDB");
    }

    public boolean adicionarFuncionarioDTO(FuncionarioDTO f) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO Funcionario (NOME) VALUES (?)");
            stmt.setString(1, f.getNome());

            int ret = stmt.executeUpdate();
            con.close();
            return (ret > 0);
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao adicionar.", ex);
        }
    }
    public boolean adicionarTicketDTO(TicketDTO t) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO TICKET (DATAEMISAO, HORAEMISAO, LIBERADO) VALUES (?,?,?)");
            Date d = new Date(t.getDataEmissaolong());
            Time ti = new Time(t.getHoraEmissaolong());
            stmt.setDate(1, d);//stmt.setString(1, t.getDataString());
            stmt.setTime(2, ti);//stmt.setString(2, t.getTimeString());
            stmt.setString(3, "N");

            int ret = stmt.executeUpdate();
            con.close();
            return (ret > 0);
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao adicionar.", ex);
        }
    }
    public boolean adicionarCartaoEspDTO(CartaoEspDTO c) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO CARTAO_ESP (COD_ESP, COD_FUNC) VALUES (?,?)");
            stmt.setInt(1, c.getCod());
            stmt.setInt(2, c.getCod_func());

            int ret = stmt.executeUpdate();
            con.close();
            return (ret > 0);
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao adicionar.", ex);
        }
    }
    public boolean adicionarCartaoUsoDTO(CartaoUsoDTO c) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO CARTAO_USO (COD_ESP, COD_FUNC, DATA_PGTO , HORA, IS_ENTRADA ) VALUES (?,?,? ,?, ?)");
            Date d = new Date(c.getDataLong());
            Time t = new Time(c.getTimeLong());
            
            stmt.setInt(1, c.getCod_esp());
            stmt.setInt(2, c.getCod_func());
            stmt.setDate(3, d);
            stmt.setTime(4, t);
            stmt.setString(5, c.getIsEntrada());

            int ret = stmt.executeUpdate();
            con.close();
            return (ret > 0);
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao adicionar.", ex);
        }
    }
    public boolean adicionarPagamentoDTO(PagamentoDTO c) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO PAGAMENTO ( ID_TICKET, VALOR, DATA_PGTO, HORA_PGTO) VALUES (?,?,? ,?)");
            
            stmt.setInt(1, c.getId());
            stmt.setDouble(2, c.getVlrPagto());
            stmt.setDate(3, new Date(c.getData().getTime()));
            stmt.setTime(4, new Time(c.getHora().getTime()));

            int ret = stmt.executeUpdate();
            con.close();
            return (ret > 0);
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao adicionar.", ex);
        }
    }

    public List<TicketDTO> getTickets() throws DAOSQLExcep{
       try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet resultado = stmt.executeQuery("SELECT ID, DATAEMISAO, LIBERADO, HORAEMISAO "
                    + "FROM TICKET "
                    + "WHERE LIBERADO <> 'S'");
            List<TicketDTO> lista = new ArrayList<>();
            while(resultado.next()) {
                int id = resultado.getInt("ID");
                Date data = resultado.getDate("DATAEMISAO");
                String liberado = resultado.getString("LIBERADO");
                Time time = resultado.getTime("HORAEMISAO");

                lista.add(new TicketDTO(id, time.getTime(),data.getTime(), liberado.charAt(0)));
            }
            return lista;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public TicketDTO GenerateTicket()  throws DAOSQLExcep{
        java.util.Date d = new java.util.Date();
        adicionarTicketDTO(new TicketDTO(0, d.getTime(), d.getTime(), 'N'));
        return getUltimo();
    }

    private TicketDTO getUltimo() throws DAOSQLExcep {
        TicketDTO ret = null;
        for(TicketDTO t : getTickets()){
            if(ret == null)
                ret = t;
            else if(t.getId() > ret.getId()){
                ret = t;
            }
        }
        return ret;
    }

    @Override
    public boolean LiberaTicket(TicketDTO t) throws DAOSQLExcep{
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "UPDATE TICKET SET LIBERADO = ? WHERE ID = ?");
            stmt.setString(1,"S");
            stmt.setInt(2, t.getId());
            
            int ret = stmt.executeUpdate();
            return (ret > 0);
        }
        catch(SQLException e){
            throw new DAOSQLExcep("Erro ao liberar ticket.", e);
        }
 
    }

    @Override
    public double getTotalEstadiaDia(String data) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT SUM(VALOR) "
                    + "FROM PAGAMENTO "
                    + "WHERE DATA_PGTO = ? ");
            stmt.setString(1, data);
            ResultSet ret = stmt.executeQuery();
                ret.next(); 
                double total = ret.getDouble(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public double getTotalEstadiaMes(String mes, String ano) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT SUM(VALOR) "
                    + "FROM PAGAMENTO "
                    + "WHERE MONTH(DATA_PGTO) = ? AND YEAR(DATA_PGTO) = ? ");
            stmt.setString(1, mes);
            stmt.setString(2, ano);
            ResultSet ret = stmt.executeQuery();
                ret.next(); 
                double total = ret.getDouble(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public int getNroTicketsPagosDia(String data) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(ID_TICKET) "
                    + "FROM PAGAMENTO "
                    + "WHERE DATA_PGTO = ? ");
            stmt.setString(1, data);
            ResultSet ret = stmt.executeQuery();
            ret.next(); 
                int total = ret.getInt(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public int getNroTicketsPagosMes(String mes, String ano) throws DAOSQLExcep {
         try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(ID_TICKET) "
                    + "FROM PAGAMENTO "
                    + "WHERE MONTH(DATA_PGTO) = ? AND YEAR(DATA_PGTO) = ? ");
            stmt.setString(1, mes);
            stmt.setString(2, ano);
            ResultSet ret = stmt.executeQuery();
            ret.next(); 
                int total = ret.getInt(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }


    
    @Override
    public int getNroTicketEspDia(String data) throws DAOSQLExcep {
         try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(COD_ESP) "
                    + "FROM CARTAO_USO "
                    + "WHERE DATA_PGTO = ? ");
            stmt.setString(1, data);
            ResultSet ret = stmt.executeQuery();
            ret.next(); 
                int total = ret.getInt(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public int getNroTicketEspMes(String mes, String ano) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(COD_ESP) "
                    + "FROM CARTAO_USO "
                    + "WHERE MONTH(DATA_PGTO) = ? AND YEAR(DATA_PGTO) = ? ");
            stmt.setString(1, mes);
            stmt.setString(2, ano);
            ResultSet ret = stmt.executeQuery();
            ret.next(); 
                int total = ret.getInt(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }
       
    @Override
    public int getNroTicketEspFuncionario(String func) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(COD_ESP) "
                    + "FROM CARTAO_USO INNER JOIN FUNCIONARIO "
                    + "ON CARTAO_USO.COD_FUNC = FUNCIONARIO.COD "
                    + "WHERE NOME = ? ");
            stmt.setString(1, func);
            ResultSet ret = stmt.executeQuery();
            ret.next(); 
                int total = ret.getInt(1);
            return total;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public boolean podeLiberar(String user, String  cartao) throws DAOSQLExcep{
        CartaoUsoDTO cartaoUso = lastCartaoUso(user, user) ;
        if(cartao == null){
            //ou pode ser para sair é necessario entrar
            throw new DAOSQLExcep("Cartão Não encontrado!!");
        }
        if(cartaoUso.getIsEntrada().equals("S"))
        {
            //registrar saida
            cartaoUso = new CartaoUsoDTO(cartaoUso.getCod_esp(), cartaoUso.getCod_func(), "N", new java.util.Date().getTime(), new java.util.Date().getTime());
            adicionarCartaoUsoDTO(cartaoUso);
            return true;
        }
        return false;
    }

    @Override
    public void registrarEntrada(String Cartao, String user) throws DAOSQLExcep, NumberFormatException{
        CartaoUsoDTO cartaoUso = lastCartaoUso(user, user) ;
        if(cartaoUso != null )
            if (cartaoUso.getIsEntrada().equals("S"))
                throw new DAOSQLExcep("É necessario registrar a saida para poder entrar!");
        if(validaCartaoEsp(Cartao, user))
        {
            int cart = Integer.parseInt(Cartao);
            int us = Integer.parseInt(user);
   
            java.util.Date dt = new java.util.Date();
            //deve registrar a entrada
            cartaoUso = new CartaoUsoDTO(cart, us, "S", dt.getTime(), dt.getTime());
            adicionarCartaoUsoDTO(cartaoUso);
            return;
        }
        throw new DAOSQLExcep("Cartão Não encontrado!!");
    }

    @Override
    public boolean podeLiberar(String ticket_cartao) throws DAOSQLExcep {
        TicketDTO t = getTicket(ticket_cartao);
        if(t.getLiberado() == 'S')
            return true;
        return false;
    }

    private boolean validaCartaoEsp(String cartao, String user) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT  COUNT(COD_ESP) "
                    + "FROM CARTAO_ESP "
                    + "WHERE COD_ESP = ? AND COD_FUNC = ? ");
            stmt.setString(1, cartao);
            stmt.setString(2, user);
            ResultSet ret = stmt.executeQuery();
            ret.next(); 
                int total = ret.getInt(1);
            return total > 0;
            
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }
    private CartaoUsoDTO lastCartaoUso(String cartao, String user) throws DAOSQLExcep{
        try {
            Connection con = getConnection();
            PreparedStatement stmt;
            
             stmt = con.prepareStatement("SELECT MAX(u.DATA_PGTO), MAX(u.HORA) "
                    + "FROM CARTAO_USO u JOIN CARTAO_ESP e ON(e.COD_FUNC = u.COD_FUNC AND e.COD_ESP = u.COD_ESP) "
                    + "WHERE e.COD_ESP = ? AND e.COD_FUNC = ?");
            
            stmt.setString(1, cartao);
            stmt.setString(2, user);
            Date maxD = null;
            Time maxt = null;
            ResultSet r = stmt.executeQuery();
                
            if(r.next()){
                maxD = r.getDate(1);
                maxt = r.getTime(2);
            }
            if(maxD == null)
                return null;
            
            stmt = con.prepareStatement("SELECT MAX(u.HORA) "
                    + "FROM CARTAO_USO u INNER JOIN CARTAO_ESP e ON(e.COD_FUNC = u.COD_FUNC AND e.COD_ESP = u.COD_ESP) "
                    + "WHERE e.COD_ESP = ? AND e.COD_FUNC = ? AND u.DATA_PGTO = ? ");
            
            stmt.setString(1, cartao);
            stmt.setString(2, user);
            stmt.setDate(3, maxD);
            r = stmt.executeQuery();
            if(r.next()){
               maxt = r.getTime(1);
            }
            stmt = con.prepareStatement("SELECT * "
                    + "FROM CARTAO_USO u INNER JOIN CARTAO_ESP e ON(e.COD_FUNC = u.COD_FUNC AND e.COD_ESP = u.COD_ESP) "
                    + "WHERE e.COD_ESP = ? AND e.COD_FUNC = ? AND u.DATA_PGTO = ? AND u.HORA = ?");
            stmt.setString(1, cartao);
            stmt.setString(2, user);
            stmt.setDate(3, maxD);
            stmt.setTime(4, maxt);
                        
            ResultSet ret = stmt.executeQuery();
            if(ret.next()){
                String isEntrada = ret.getString("IS_ENTRADA");
                int cod_esp = ret.getInt("COD_ESP");
                int cod_user = ret.getInt("COD_FUNC");
                return new CartaoUsoDTO(cod_esp, cod_user, isEntrada, maxD.getTime(), maxt.getTime());
            }
            return null;
            
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    private TicketDTO getTicket(String ticket_cartao) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT * "
                    + "FROM TICKET "
                    + "WHERE ID = ?");
            stmt.setString(1, ticket_cartao);
            ResultSet ret = stmt.executeQuery();
            if(ret.next()){
                int isEntrada = ret.getInt("ID");
                Date d = ret.getDate("DATAEMISAO");
                Time t = ret.getTime("HORAEMISAO");
                String liberado = ret.getString("LIBERADO");
                return new TicketDTO(isEntrada, t.getTime(), d.getTime(), liberado.charAt(0));
            }
            return null;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Falha ao buscar.", ex);
        }
    }

    @Override
    public String gerarFuncionario(String nome) throws DAOSQLExcep{
        FuncionarioDTO f = new FuncionarioDTO(0, nome);
        adicionarFuncionarioDTO(f);
        f = getLastFuncionario();
        return f.getId()+"";
    }

    private FuncionarioDTO getLastFuncionario() throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * "
                    + "FROM FUNCIONARIO "
                    +" WHERE COD = (SELECT MAX(COD) "
                    + "FROM FUNCIONARIO )");
            if(result.next()){
                int id = result.getInt(1);
                String nome = result.getString("nome");
                return new FuncionarioDTO(id, nome);
            }
            return null;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Ocorreu um erro ao buscar os funcionarios.");
        }
    }

    @Override
    public String getLastTicket() throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery("SELECT max(ID) "
                    + "FROM TICKET "
                    );
            if(result.next()){
                int id = result.getInt(1);
                return id+"";
            }
            return null;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Ocorreu um erro ao buscar o ticket.");
        }
    }

    @Override
    public List<FuncionarioDTO> getFuncs() throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * "
                    + "FROM FUNCIONARIO ");
            List<FuncionarioDTO> ret = new ArrayList<>();
            while(result.next()){
                int id = result.getInt(1);
                String nome = result.getString("nome");
                ret.add(new FuncionarioDTO(id, nome));
            }
            return ret;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("Ocorreu um erro ao buscar os funcionarios.");
        }
        
    }

    @Override
    public void pagou(TicketDTO toModel, double total) throws DAOSQLExcep{
        java.util.Date d = new java.util.Date();
        PagamentoDTO p = new PagamentoDTO(toModel.getId(), total, d.getTime(), d.getTime());
        adicionarPagamentoDTO(p);
        //LiberaTicket(toModel);
    }

    @Override
    public void createCartaoEsp(FuncionarioDTO func) throws DAOSQLExcep {
        int qtdCart = getQtdCartEsp(func);
        if(qtdCart >= 3){
            throw new DAOSQLExcep("Limite de cartões especiais excedido.");
        }
        CartaoEspDTO c = new CartaoEspDTO(qtdCart + 1, func.getId());
        adicionarCartaoEspDTO(c);
    }

    private int getQtdCartEsp(FuncionarioDTO func) throws DAOSQLExcep {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(COD_ESP) "
                    + "FROM CARTAO_ESP "
                    + "WHERE COD_FUNC = ?");
            stmt.setInt(1, func.getId());
            ResultSet resp = stmt.executeQuery();
            if(resp.next()){
                return resp.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            throw new DAOSQLExcep("ocorreu um erro ao buscar os cartões especiais.");
        }
    }



    
}
