/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;

import java.util.Date;


/**
 *
 * @author Brike
 */
public class PagamentoDTO {
    private int id;
    private double vlrPagto;
    private Date data;
    private Date hora;

    public PagamentoDTO(int id, double vlrPagto, long data, long hora) {
        this.id = id;
        this.vlrPagto = vlrPagto;
        this.data = new Date( data);
        this.hora = new Date(hora);
    }

    public Date getHora() {
        return (Date)hora.clone();
    }


    public Date getData() {
        return (Date)data.clone();
    }

    public int getId() {
        return id;
    }

    public double getVlrPagto() {
        return vlrPagto;
    }
    /*
    public String getDataString(){
        return data.getYear()+"-"+data.getMonth()+"-"+data.getDayOfMonth();
    }
    
    public String getTimeString(){
        return data.getHours()+":"+data.getMinutes()+":"+data.getSeconds();
    }
    */
    
}
