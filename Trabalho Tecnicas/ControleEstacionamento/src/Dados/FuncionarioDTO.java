/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dados;

/**
 *
 * @author Brike
 */
public class FuncionarioDTO {
    
    private int id;
    private String nome;

    public FuncionarioDTO(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
