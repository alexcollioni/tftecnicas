/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.EventListener;

/**
 *
 * @author Brike
 */
public interface EdicaoListener extends EventListener{
    void elementoEditado(EdicaoEvent evt);
}

