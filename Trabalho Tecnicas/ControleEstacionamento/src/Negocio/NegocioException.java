/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;




/**
 *
 * @author Brike
 */
public class NegocioException extends Exception {
    
    public NegocioException(String msg) {
        super(msg);
    }
    
    public NegocioException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
