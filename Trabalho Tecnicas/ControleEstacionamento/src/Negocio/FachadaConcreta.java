/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Negocio_Dados.Funcionario;
import Negocio_Dados.Negocio2Dados;
import Negocio_Dados.Negocio2DadosConcreta;
import java.text.SimpleDateFormat;
import java.util.*;
import org.joda.time.Period;
import org.joda.time.chrono.GregorianChronology;

/**
 *
 * @author Brike
 */
public class FachadaConcreta implements FachadaNegocio{
    private Negocio2Dados mc;
    private List<CadastroListener> listenerCadastro;
    private List<EdicaoListener> listenerEdicao;
    private List<RemoveListener> listenerRemove;
    private List<EdicaoFuncListener> listenerEditFunc;
    private List<AddFuncListner> listenerAddFunc;
   
    private static FachadaConcreta instance;
    public static FachadaConcreta getInstance() throws NegocioException{
        if(instance == null)
            instance = new FachadaConcreta();
        return instance;
    }
    
    private FachadaConcreta() throws NegocioException, NegocioException{
        listenerCadastro = new ArrayList<>();
        listenerEdicao = new ArrayList<>();
        listenerRemove = new ArrayList<>();
        listenerEditFunc = new ArrayList<>();
        listenerAddFunc = new ArrayList<>();
        mc = Negocio2DadosConcreta.getInstance();
    }
    

    @Override
    public void addCadastroListener(CadastroListener model) {

        if(!listenerCadastro.contains(model)){
            listenerCadastro.add(model);
        }
    }

    @Override
    public void addEditListener(EdicaoListener model) {
        if(!listenerEdicao.contains(model)){
            listenerEdicao.add(model);
        }
    }
    
    @Override
    public void addRemoverListener(RemoveListener model) {
        if(!listenerRemove.contains(model)){
            listenerRemove.add(model);
        }
    }

    @Override
    public void GenerateTicket() throws NegocioException  {
        Ticket t = mc.GenerateTicket();
        eventoAdicaoTicket(t);
    }

    private void eventoAdicaoTicket(Ticket t) {
        CadastroEvent evt = new CadastroEvent(this, t);
        for(CadastroListener c : listenerCadastro){
            c.elementoAdicionado(evt);
        }
    }

    @Override
    public List<Ticket> getTickets() throws NegocioException{
        return mc.getTickets();
    }

    @Override
    public void liberaTicket(Ticket t) throws NegocioException{
        mc.LiberaTicket(t);
        DisparaRemocao(t);
    }
    
    private void DisparaRemocao(Ticket t){
        RemoveEvent evt = new RemoveEvent(this, t);
        for(RemoveListener l : listenerRemove)
            l.elementoRemovido(evt);
    }
    
    @Override
    public double ExecutaPagamento(Ticket t) throws NegocioException{
        
                
        Calendar cini = Calendar.getInstance();
        Calendar cfin = Calendar.getInstance();
        
        cini.setTime(t.getHoraEntrada());
        cfin.setTime(new Date());
        
        long milli_ini = cini.getTimeInMillis();
        long milli_fin = cfin.getTimeInMillis();
        double total = 0;
        
        int dia_entrada = cini.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        int dia_saida = cini.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        int hora_saida = cfin.get(Calendar.HOUR_OF_DAY);

        org.joda.time.Period p = new Period( milli_ini, milli_fin, GregorianChronology.getInstance() );
        
        System.out.println(dia_entrada+" "+ dia_saida+" "+hora_saida);
        if(dia_entrada <= dia_saida - 1 && hora_saida > 8){
            //paga 50R$
            int diarias = (dia_saida - dia_entrada) * 50;
            total = diarias * 50.0;
        }
        if(p.getMinutes() < 20 && p.getHours() == 0){
            //isento
            total = 0;
        }
        else if(p.getHours() < 3  )
        {   //cobrar 3,5R$
            total = 3.5;
        }
        else {
            //50R$
            total = 10.0;
        }
        mc.pagou(t, total);
        return total;
    }

    @Override
    public double getTotalEstadiaDia(String s) throws NegocioException {
        return mc.getTotalEstadiaDia(s);
    }

    @Override
    public double getTotalEstadiaMes(String mes, String ano) throws NegocioException {
        return mc.getTotalEstadiaMes(mes,ano);
    }

    @Override
    public int getNroTicketsPagosDia(String s) throws NegocioException {
        return mc.getNroTicketsPagosDia(s);
    }

    @Override
    public int getNroTicketsPagosMes(String mes, String ano) throws NegocioException {
        return mc.getNroTicketsPagosMes(mes,ano);
    }

    @Override
    public int getNroTicketEspDia(String s) throws NegocioException {
        return mc.getNroTicketEspDia(s);
    }

    @Override
    public int getNroTicketEspMes(String mes, String ano) throws NegocioException {
        return mc.getNroTicketEspMes(mes,ano);
    }

    @Override
    public int getNroTicketEspFuncionario(String s) throws NegocioException {
        return mc.getNroTicketEspFuncionario(s);
    }

    @Override
    public boolean podeLiberar(String ticket_cartao) throws NegocioException {
        if(ticket_cartao.contains("-")){
            String aux[] = ticket_cartao.split("-");
            return mc.podeLiberar(aux[1], aux[0]);
        }
        return mc.podeLiberar(ticket_cartao);
    }

    @Override
    public void registraEntrada(String numCartao, String user) throws NegocioException {
        mc.registrarEntrada(numCartao, user);
    }

    @Override
    public String gerarFuncionario(String nome) throws NegocioException {
        String aux = mc.gerarFuncionario(nome);
        int id = Integer.parseInt(aux);
        Funcionario f = new Funcionario(id, nome);
        addFunc(f);
        
        return aux;
    }

    @Override
    public String getLastTicket() throws NegocioException {
        return mc.getLastTicket();
    }

    @Override
    public List<Funcionario> getFuncs() throws NegocioException{
        return mc.getFuncs();
    }

    @Override
    public void addCadFuncListener(AddFuncListner modelFunc) {
        if(!listenerAddFunc.contains(modelFunc)){
            listenerAddFunc.add(modelFunc);
        }
    }

    @Override
    public void addEditFuncListener(EdicaoFuncListener modelFunc) {
        if(!listenerEditFunc.contains(modelFunc)){
            listenerEditFunc.add(modelFunc);
        }
    }

    private void addFunc(Funcionario f) {
        FuncEvent evt = new FuncEvent(this, f);
        for( AddFuncListner l : listenerAddFunc){
            l.funcAdd(evt);
        }
    }

    @Override
    public double extravio(Ticket t) throws NegocioException{
        mc.pagou(t, 50.0);
        mc.LiberaTicket(t);
        DisparaRemocao(t);
        return 50.0;
    }

    @Override
    public void liberaSemPagamento(Ticket t)  throws NegocioException{
        mc.pagou(t,0.0);
        mc.LiberaTicket(t);
        DisparaRemocao(t);
    }

    @Override
    public void createCartaoEsp(Funcionario funcionario) throws NegocioException {
        mc.createCartaoEsp(funcionario);
    }
    
}
/*
        
        System.out.println(cini.get(Calendar.MINUTE)+" "+ cini.get(Calendar.HOUR) +" "+ cini.get(Calendar.HOUR_OF_DAY));
        System.out.println(cfin.get(Calendar.MINUTE)+" "+ cfin.get(Calendar.HOUR) +" "+ cfin.get(Calendar.HOUR_OF_DAY));
        System.out.println(dif.get(Calendar.MINUTE)+" "+ dif.get(Calendar.HOUR) +" "+ dif.get(Calendar.HOUR_OF_DAY));
        System.out.println("----------------");
        
        long segundos = (t.getHoraEntrada().getTime() - 
        (new Date()).getTime()) / 1000;
      int semanas = (int)Math.floor(segundos / 604800);
      segundos -= semanas * 604800;
      int dias = (int)Math.floor(segundos / 86400);
      segundos -= dias * 86400;
      int horas = (int)Math.floor(segundos / 3600);
      segundos -= horas * 3600;
      int minutos = (int)Math.floor(segundos / 60);
      segundos -= minutos * 60;
        
      System.out.println("As duas datas tem " +
        (horas +dias*24)+ " horas, " + minutos + " minutos e " +
        segundos + " segundos de diferença");
        */