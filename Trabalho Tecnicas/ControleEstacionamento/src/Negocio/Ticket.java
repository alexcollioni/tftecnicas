/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.sql.SQLData;
import java.util.Calendar;
import java.util.Date;
import org.apache.derby.impl.sql.execute.SqlXmlExecutor;



/**
 *
 * @author Brike
 */
public class Ticket {
    public int codigo;
    public Date dataEntrada;
    public Date horaEntrada;

    public Ticket(int codigo, long dataEntrada ,long horaEntrada) {
        this.codigo = codigo;
        this.dataEntrada = new Date(dataEntrada);
        this.horaEntrada = new Date(horaEntrada);
        
    }

    public Date getDataEntrada() {
        return (Date)dataEntrada.clone();
    }

    public Date getHoraEntrada() {
        return (Date)horaEntrada.clone();
    }

    public long getDataEntradaLong() {
        return dataEntrada.getTime();
    }

    public long getHoraEntradaLong() {
        return horaEntrada.getTime();
    }

    public int getCodigo() {
        return codigo;
    }
    
    public java.sql.Date getSqlDate(){
        return new java.sql.Date(getDataEntradaLong());
    }
    
    
    public java.sql.Time getSqlTime(){
        return new java.sql.Time(getHoraEntradaLong());
    
    }
    
    
    
    
}
