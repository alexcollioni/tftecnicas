/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Negocio_Dados.Funcionario;
import java.util.List;

/**
 *
 * @author Brike
 */
public interface FachadaNegocio {
    List<Ticket> getTickets() throws NegocioException;

    public void addCadastroListener(CadastroListener model);

    public void addEditListener(EdicaoListener model);

    void GenerateTicket() throws NegocioException;

    public void liberaTicket(Ticket t) throws NegocioException;
    
    public void addRemoverListener(RemoveListener model);
    
    public double ExecutaPagamento(Ticket t) throws NegocioException;

    public double getTotalEstadiaDia(String s) throws NegocioException;

    public double getTotalEstadiaMes(String mes, String ano) throws NegocioException;

    public int getNroTicketsPagosDia(String s) throws NegocioException;

    public int getNroTicketsPagosMes(String mes, String ano) throws NegocioException;

    public int getNroTicketEspDia(String s) throws NegocioException;

    public int getNroTicketEspMes(String mes, String ano) throws NegocioException;

    public int getNroTicketEspFuncionario(String s) throws NegocioException;

    public boolean podeLiberar(String ticket_cartao) throws NegocioException ;

    public void registraEntrada(String numCartao, String user) throws NegocioException ;

    public String gerarFuncionario(String nome) throws NegocioException;

    public String getLastTicket() throws NegocioException;
    
    public List<Funcionario> getFuncs()throws NegocioException;

    public void addCadFuncListener(AddFuncListner modelFunc);

    public void addEditFuncListener(EdicaoFuncListener modelFunc);

    public double extravio(Ticket t) throws NegocioException;

    public void liberaSemPagamento(Ticket t) throws NegocioException;

    public void createCartaoEsp(Funcionario funcionario) throws NegocioException;

}
