/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.EventListener;

/**
 *
 * @author Brike
 */
public interface RemoveListener extends EventListener{
    void elementoRemovido(RemoveEvent evt);
}

