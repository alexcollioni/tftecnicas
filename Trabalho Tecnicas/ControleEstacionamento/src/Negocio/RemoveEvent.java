/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.EventObject;

/**
 *
 * @author Brike
 */
public class RemoveEvent extends EventObject{
        private Ticket ticket;
    
    public RemoveEvent(Object source, Ticket p) {
        super(source);
        ticket = p;
    }
    
    public Ticket getPessoa() {
        return ticket;
    }
}
