/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Negocio.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Brike
 */
public class ListaTicketsModel extends DefaultTableModel implements CadastroListener, EdicaoListener, RemoveListener{
    private List<Ticket> dados;
    
    public ListaTicketsModel(){
        dados = new ArrayList<>();
        String col[] = {"Codigo","Data","Hora"};
        this.setColumnIdentifiers(col);
    }
        
    public ListaTicketsModel(List<Ticket> aux){
        this();
        dados.addAll(aux);
    }
    
    @Override
    public Vector getDataVector(){
        return convertToVector(dados.toArray());
    
    }
    
    @Override
    public boolean isCellEditable(int row, int col){
        return false;
    }
    
    @Override
    public int getRowCount() {
        if(dados == null)
            dados = new ArrayList<>();
        return dados.size();
        
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Ticket t = dados.get(rowIndex);
        GregorianCalendar data = new GregorianCalendar();
        data.setTime(t.getDataEntrada());

        GregorianCalendar hora = new GregorianCalendar();
        hora.setTimeInMillis(t.getHoraEntrada().getTime());
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfHora = new SimpleDateFormat("hh:mm aaa");
        
        switch(columnIndex){
            case 0: return t.getCodigo();
            case 1: return sdf.format(data.getTime());
            case 2: return sdfHora.format(hora.getTime());
        }
        return rowIndex;
    }

    @Override
    public void elementoAdicionado(CadastroEvent evt) {
        Ticket t = evt.getPessoa();
        dados.add(t);
        fireTableRowsInserted(dados.size(), dados.size());
    }

    @Override
    public void elementoEditado(EdicaoEvent evt) {
        Ticket ret = null;
        for(Ticket t : dados){
            if(t.getCodigo() ==evt.getPessoa().getCodigo()){
                ret = t;
            }
        }
        int pos = dados.indexOf(ret);
        fireTableRowsUpdated(pos, pos);
    }

    @Override
    public void elementoRemovido(RemoveEvent evt) {
        Ticket ret = null;
        for(Ticket t : dados){
            if(t.getCodigo() == evt.getPessoa().getCodigo()){
                ret = t;
            }
        }
        int pos = dados.indexOf(ret);
        dados.remove(pos);
        fireTableRowsDeleted(pos, pos);
    }
    
}
