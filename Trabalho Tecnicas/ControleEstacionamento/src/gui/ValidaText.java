/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author Brike
 */
public class ValidaText {
    
    
  public static String validaDia(String s){     
        String res = validaData(s);
           if (res == null)
                return null;
             return res;
    }
  
    static String validaMes(String s){
        String r = validaData(s);
        if (r == null)
             return null;
        StringBuilder string = new StringBuilder();
        string.append(r.charAt(5));
        string.append(r.charAt(6));
        return string.toString();
    }
    
    static String validaAno(String s){
        String r = validaData(s);  
        if (r == null)
             return null;
        StringBuilder string = new StringBuilder();
        string.append(r.charAt(0));
        string.append(r.charAt(1));
        string.append(r.charAt(2));
        string.append(r.charAt(3));
        return string.toString();
    }
    
    static String validaFuncionario(String s){
        if (s.length() == 0 || s.length() > 100) {
            return null;
        }
        if(!(s.matches("[a-zA-Z]+")))
            return null;
        return s; 

    }
    
    
    private static String validaData(String s) {
        if (s.length() < 10 || s.length() > 10) {
            return null;
        }
        StringBuilder string = new StringBuilder(s);
        string.replace(2, 3, "");
        string.replace(4, 5, "");
        try {
            int i = Integer.parseInt(string.toString());         
            
        } catch (NumberFormatException nfe) {
            return null;
        }
        
        int dia = Integer.parseInt(string.substring(0, 2));
        if (dia < 1 || dia > 31){
            return null;
        }
        int mes = Integer.parseInt(string.substring(2, 4));
        if (mes < 1 || mes > 12){
            return null;
        }
        
        int ano = Integer.parseInt(string.substring(4, 8));
           if (ano < 1900 || ano > 9999){
            return null;
        }
           
           
        StringBuilder resb = new StringBuilder();
        resb.append(ano);     
        resb.append('-');
        if(mes < 10)
        resb.append('0');
        resb.append(mes);   
        resb.append('-');  
        if(dia < 10)
        resb.append('0');
        resb.append(dia);        
        return resb.toString();

    }
    
 
}
