/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Negocio.FachadaConcreta;
import Negocio.FachadaNegocio;
import Negocio.NegocioException;
import Negocio.Ticket;
import Negocio_Dados.Funcionario;
import javax.swing.ListModel;


/**
 *
 * @author Brike
 */
public class ControladorTelas {
    private ListaTicketsModel modelTicket;
    private ListaFuncModel modelFunc;
    private FachadaNegocio fachada;
    private static ControladorTelas instance;
    public static ControladorTelas getInstance() throws NegocioException{
        if(instance == null)
            instance = new ControladorTelas();
        return instance;
    }
    
    
    private ControladorTelas() throws NegocioException{
        //instancia a fachada
        fachada =FachadaConcreta.getInstance();
        //inicializa o model com os dados do banco (que vem pela fachada)
        modelTicket = new ListaTicketsModel(fachada.getTickets());
        modelFunc = new ListaFuncModel(fachada.getFuncs());        
        fachada.addCadastroListener(modelTicket);
        fachada.addEditListener(modelTicket);
        fachada.addRemoverListener(modelTicket);
        fachada.addCadFuncListener(modelFunc);
        fachada.addEditFuncListener(modelFunc);
    }
    
    public ListaTicketsModel getListaTicketModel(){
        return modelTicket;
    }

    void generateTicket() throws NegocioException {
        fachada.GenerateTicket();
    }

    double liberaTicket(int rowSelec) throws NegocioException{
        Ticket t = getTicket(rowSelec);
        double ret = fachada.ExecutaPagamento(t);
        fachada.liberaTicket(t);
        return ret;
    }
    
    private Ticket getTicket(int row){
        return (Ticket)modelTicket.getDataVector().get(row);
    }

    double getTotalEstadiaDia(String s) throws NegocioException {
       return fachada.getTotalEstadiaDia(s);
    }

    double getTotalEstadiaMes(String mes, String ano) throws NegocioException {
        return fachada.getTotalEstadiaMes(mes,ano);
    }

    int getNroTicketsPagosDia(String s) throws NegocioException {
        return fachada.getNroTicketsPagosDia(s);
    }

    int getNroTicketsPagosMes(String mes, String ano) throws NegocioException {
        return fachada.getNroTicketsPagosMes(mes,ano);
    }

    int getNroTicketEspDia(String s) throws NegocioException {
        return fachada.getNroTicketEspDia(s);
    }

    int getNroTicketEspMes(String mes, String ano) throws NegocioException {
        return fachada.getNroTicketEspMes(mes,ano);
    }

    int getNroTicketEspFuncionario(String s) throws NegocioException {
        return fachada.getNroTicketEspFuncionario(s);
    }

    void registraEntrada(String numCartao,String user) throws NegocioException {
        
        fachada.registraEntrada(numCartao, user);
    }

    boolean podeLiberar(String ticket_cartao) throws NegocioException {
        return fachada.podeLiberar(ticket_cartao);
    }

    String gerarFuncionario(String nome) throws NegocioException {
        return fachada.gerarFuncionario(nome);
    }

    String getLastTicket() throws NegocioException {
        return fachada.getLastTicket();
    }

    ListModel getListaFuncModel() {
        return modelFunc;
    }

    double extravio(int rowSelec) throws NegocioException {
        Ticket t = (Ticket)modelTicket.getDataVector().get(rowSelec);
        double aux = fachada.extravio(t);
        return aux;
    }

    void liberaTicketSemPgto(int rowSelec) throws NegocioException{
       Ticket t = (Ticket) modelTicket.getDataVector().get(rowSelec);
       
       fachada.liberaSemPagamento(t);
       
    }

    void createCartaoEsp(Funcionario funcionario) throws NegocioException {
        fachada.createCartaoEsp(funcionario);
    }
    
    
}
