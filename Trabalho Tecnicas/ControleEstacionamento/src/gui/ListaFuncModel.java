/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Negocio.FuncEvent;
import Negocio_Dados.Funcionario;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author Alex
 */
public class ListaFuncModel extends AbstractListModel implements Negocio.EdicaoFuncListener, Negocio.AddFuncListner{

    private List<Funcionario> model;

    ListaFuncModel(List<Funcionario> funcs) {
        model = funcs;
    }
    @Override
    public int getSize() {
        return model.size();
    }

    @Override
    public Object getElementAt(int i) {
         return model.get(i);
    }

    @Override
    public void funcEditado(FuncEvent ev) {
        for(int i = 0; i < model.size(); i++)
        {
            Funcionario aux = model.get(i);
            if(aux.getId() == ev.getF().getId()){
                model.set(i, aux);
                fireContentsChanged(this, i, i);
        }
        }
    }

    @Override
    public void funcAdd(FuncEvent f) {
        model.add(f.getF());
        fireIntervalAdded(this, model.size(), model.size());
    }
    
}
