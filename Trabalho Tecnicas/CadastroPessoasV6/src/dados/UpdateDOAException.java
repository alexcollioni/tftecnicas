/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

/**
 *
 * @author Alex
 */
public class UpdateDOAException extends Exception{
    
    /**
     * Creates a new instance of
     * <code>UpdateDOAException</code> without detail message.
     */
    public UpdateDOAException() {
    }

    /**
     * Constructs an instance of
     * <code>UpdateDOAException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public UpdateDOAException(String msg) {
        super(msg);
    }

    public UpdateDOAException(String message, Throwable cause) {
        super(message, cause);
    }
}
