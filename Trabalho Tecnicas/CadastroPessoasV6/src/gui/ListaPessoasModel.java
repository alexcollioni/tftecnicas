/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import negocio.*;

/**
 *
 * @author Julio
 */
public class ListaPessoasModel extends AbstractListModel implements CadastroListener, EdicaoListener{
    private List<Pessoa> texto = new ArrayList<Pessoa>();
    
    public ListaPessoasModel(){
        super();
    }
    
    public ListaPessoasModel(List<Pessoa> dados){
        texto.addAll(dados);
    }
    
    @Override
    public int getSize() {
        return texto.size();
    }

    @Override
    public Pessoa getElementAt(int index) {
        return texto.get(index);
    }
    public void remove(Pessoa p){
        int pos = texto.indexOf(p);
        texto.remove(pos);
        fireIntervalRemoved(this, pos, pos);
    
    }
    
    public void add(Pessoa p) {
        texto.add(p);
        fireIntervalAdded(this, texto.size(), texto.size());
    }

    @Override
    public void elementoAdicionado(CadastroEvent evt) {
        add(evt.getPessoa());
    }

    @Override
    public void elementoEditado(EdicaoEvent evt) {
        Pessoa p = evt.getPessoa();
        Pessoa aux;
        for(int i = 0; i < texto.size(); i++){
            aux = texto.get(i);
            if(aux.getId() == p.getId()){
                texto.set(i, p);
                fireContentsChanged(this, i, i);
                
                /*
                texto.remove(aux);
                add(p);*/
                return;
            }
        }
    }
}
