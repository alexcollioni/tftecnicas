/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ListModel;
import negocio.*;

/**
 *
 * @author Julio
 */
public class CadastroControlador {
    private CadastroFachada fachada;
    private ListaPessoasModel modelSaidaTexto;

    public CadastroControlador() throws CadastroException {
        fachada = new CadastroFachada();
        modelSaidaTexto = new ListaPessoasModel(fachada.buscarTodos());
        fachada.addCadastroListener(modelSaidaTexto);
        fachada.addEdicaoListener(modelSaidaTexto);
    }

    public ListModel getListaPessoasModel(){
        return modelSaidaTexto;
    }
    /*
    private List<String> toListString(List<Pessoa> listaOrigem) {
        List<String> listaDestino = new ArrayList<String>(listaOrigem.size());
        for(Pessoa p : listaOrigem) {
            listaDestino.add(p.toString());
        }
        return listaDestino;
    }*/
    public boolean editarPessoa(Pessoa p) throws EdicaoException{
        return fachada.editarPessoa(p);
    }
    public boolean adicionarPessoa(String nome, String telefone, boolean masculino) throws CadastroException {
        Pessoa p = fachada.adicionarPessoa(nome, telefone, masculino);
        if(p != null){
            return true;
        }
        return false;
    }

    public List<Pessoa> buscarHomens() throws CadastroException {
        return (fachada.buscarHomens());
    }

    public List<Pessoa> buscarMulheres() throws CadastroException {
        return (fachada.buscarMulheres());
    }

    public List<Pessoa> getTodos() throws CadastroException {
        return (fachada.buscarTodos());
    }
}
