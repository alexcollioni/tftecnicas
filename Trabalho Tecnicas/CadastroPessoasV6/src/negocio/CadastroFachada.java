/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import dados.CadastroDAOException;
import dados.CadastroDAOJavaDb;
import dados.UpdateDOAException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julio
 */
public class CadastroFachada {
    private CadastroDAO dao;
    private List<CadastroListener> listenersCadastros;
    private List<EdicaoListener> listenersEdicao;
    
    
    public CadastroFachada() throws CadastroException {
        try {
            dao = CadastroDAOJavaDb.getInstance();
            listenersCadastros = new ArrayList<CadastroListener>();
            listenersEdicao = new ArrayList<EdicaoListener>();
        } catch (CadastroDAOException e) {
            throw new CadastroException("Falha de criação da fachada!", e);
        }
    }
    
    public void addCadastroListener(CadastroListener l) {
        if(!listenersCadastros.contains(l)) {
            listenersCadastros.add(l);
        }
    }
    
    public void removeCadastroListener(CadastroListener l) {
        listenersCadastros.remove(l);
    }
    
    public void addEdicaoListener(EdicaoListener l) {
        if(!listenersEdicao.contains(l)) {
            listenersEdicao.add(l);
        }
    }
    
    public void removeEdicaoListener(EdicaoListener l) {
        listenersEdicao.remove(l);
    }
    
    protected void fireElementoAdicionado(Pessoa p) {
        CadastroEvent evt = new CadastroEvent(this, p);
        for(CadastroListener l : listenersCadastros) {
            l.elementoAdicionado(evt);
        }
    }
    
    
    protected void fireElementoEditado(Pessoa p) {
        EdicaoEvent evt = new EdicaoEvent(this, p);
        for(EdicaoListener l : listenersEdicao) {
            l.elementoEditado(evt);
        }
    }
    
    public boolean editarPessoa(Pessoa p) throws EdicaoException {
        if(!ValidadorPessoa.validaNome(p.getNome())){
            throw new EdicaoException("Nome de pessoa inválido!");
        }
        if(!ValidadorPessoa.validaTelefone(p.getTelefone())) {
            throw new EdicaoException("Número de telefone inválido!");
        }        
        try {
            boolean ok = dao.update(p);
            if(ok){
                //lança evento de edição ocorrida
                fireElementoEditado(p);
                return true;
            }
        } catch (UpdateDOAException ex) {
            Logger.getLogger(CadastroFachada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }
    
    
    public Pessoa adicionarPessoa(String nome, String telefone, boolean masculino) throws CadastroException{
        if(!ValidadorPessoa.validaNome(nome)) {
            throw new CadastroException("Nome de pessoa inválido!");
        }
        if(!ValidadorPessoa.validaTelefone(telefone)) {
            throw new CadastroException("Número de telefone inválido!");
        }
        Pessoa p = new Pessoa(0,nome, telefone, masculino);
        try {
            boolean ok = dao.adicionar(p);
            if(ok) {
                fireElementoAdicionado(p);
                return p;
            }
            return null;
        } catch (CadastroDAOException e) {
            throw new CadastroException("Falha ao adicionar pessoa!", e);
        }
    }

    public List<Pessoa> buscarHomens() throws CadastroException{
        try {
            return dao.getHomens();
        } catch (CadastroDAOException e) {
            throw new CadastroException("Falha ao buscar homens!", e);
        }
    }

    public List<Pessoa> buscarMulheres() throws CadastroException{
        try {
            return dao.getMulheres();
        } catch (CadastroDAOException e) {
            throw new CadastroException("Falha ao buscar mulheres!", e);
        }
    }

    public List<Pessoa> buscarTodos() throws CadastroException{
        try {
            return dao.getTodos();
        } catch (CadastroDAOException e) {
            throw new CadastroException("Falha ao buscar pessoas!", e);
        }
    }

    public Pessoa buscarPessoaPorNome(String n) throws CadastroException{
        try{
            return dao.getPessoaPorNome(n);
        } catch(CadastroDAOException e) {
            throw new CadastroException("Falha ao buscar pessoa", e);
        }
    }
}
