package negocio;

public class Pessoa {
    private int id;
    private String nome;
    private String telefone;
    private char sexo;

    public Pessoa(int id, String umNome, String umTelefone, boolean masculino) {
        this.id = id;
        nome = umNome;
        telefone = umTelefone;
        if (masculino) {
            sexo = 'M';
        } else {
            sexo = 'F';
        }
    }

    public String getNome() {
        return nome;
    }
    
    public int getId(){
        return id;
    }

    public String getTelefone() {
        return telefone;
    }
    
    public char getSexo() {
        return sexo;
    }

    @Override
    public String toString() {
        return "[" + nome + "," + telefone + "," + sexo + "]";
    }
}
