/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

/**
 *
 * @author Alex
 */
public class EdicaoException extends Exception{
    
    /**
     * Creates a new instance of
     * <code>EdicaoException</code> without detail message.
     */
    public EdicaoException() {
    }

    /**
     * Constructs an instance of
     * <code>CadastroException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public EdicaoException(String msg) {
        super(msg);
    }

    public EdicaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
