/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.EventObject;

/**
 *
 * @author Alex
 */
public class EdicaoEvent extends EventObject{
    private Pessoa pessoa;
    
    
    public EdicaoEvent(Object source, Pessoa p) {
        super(source);
        pessoa = p;
    }
    
    public Pessoa getPessoa() {
        return pessoa;
    }
    
}
