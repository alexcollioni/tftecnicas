/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.EventListener;

/**
 *
 * @author Alex
 */
public interface EdicaoListener extends EventListener{
    void elementoEditado(EdicaoEvent evt);
}
